#!/bin/bash
file="times.txt"
if [ -f "$file" ] ; then
    rm "$file"
fi
for rank in {1..14};do
	mpirun -n $rank ./a.out
	str=$(<./outPc++/time.txt)
	echo "${rank},${str}">>$file
done;
