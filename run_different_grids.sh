#!/bin/bash
dir="times"
for i in {13..13};do
file="${dir}/times_grids${i}.txt"
if [ -f "$file" ] ; then
    rm "$file"
fi

grid=$((2**$i))
line="#define NX_GLOB ${grid}"
sed -i "57s/.*/${line}/" parAdv.cpp
mpicxx -DPARALLEL -lm -std=c++11 parAdv.cpp
	for rank in {4,9,14};do
        	mpirun -n $rank ./a.out
        	str=$(<./outPc++/time.txt)
        	echo "${rank},${str}">>$file
	done;
done;
