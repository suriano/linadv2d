/* ///////////////////////////////////////////////////////////////////// */
/*!
  \file
  \brief Upwind scheme for the 2D scalar linear advection equation
         on 2D Cartesian domain in serial/parallel.

  The methods used in this code are taken from the course
  Introduction to Parallel Programming with MPI, a.y. 2022/2023
  held by Prof. A. Mignone at Università di Torino.


  The function DomainDecomposition() is used to decompose the
  computational domain. It is general for any number of cores and grid sizes.


  The function BoundaryConditions() is used to assign both
  physical and inter-processor boundary conditions.


  The function WriteSolution() writes binary data to disk
  (fully parallel I/O is used in the parallel version).


  For the serial version, compile with

  > gcc parAdv.c

  and run with

  ./a.out


  For the parallel version, compile with

  > mpicxx -DPARALLEL -lm -std=c++11 parAdv.c  (or turn #define PARALLEL)

  and run with

  > mpirun -n nprocs ./a.out

  where nprocs is the number of cores you want to use.


  \author Alessio Suriano (alessio.suriano@untio.it)
  \date   Agust 2023
*/
/* ///////////////////////////////////////////////////////////////////// */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "tools.cpp"
#include <chrono>
using namespace std::chrono;

// #define PARALLEL
#define NX_GLOB 1024
#define NY_GLOB NX_GLOB /* Global number of interior points */
#define NDIM 2
#define NGHOST 1

#ifdef PARALLEL
#include <mpi.h>
MPI_Comm MPI_COMM_CART;
#endif

/*! Return the maximum between two numbers. */
#define MAX(a, b) ((a) >= (b) ? (a) : (b))

/*! Return the minimum between two numbers. */
#define MIN(a, b) ((a) <= (b) ? (a) : (b))

typedef struct MPI_Decomp_
{
  int nprocs[NDIM];  /*  Number of processors in each dimension */
  int periods[NDIM]; /*  Periodicity flag in each dimension     */
  int coords[NDIM];  /*  Cartesian coordinate in the MPI topology */
  int gsize[NDIM];   /*  Global domain size (no ghosts)  */
  int lsize[NDIM];   /*  Local domain size (no ghosts)   */
  int start[NDIM];   /*  Local start index in each dimension           */
  int procL[NDIM];   /*  Rank of left-lying  process in each direction */
  int procR[NDIM];   /*  Rank of right-lying process in each direction */
  int rank;          /*  Local process rank */
  int size;          /*  Communicator size  */
} MPI_Decomp;

void BoundaryConditions(double **, double *, double *, int, int, MPI_Decomp *);
void DomainDecomposition(MPI_Decomp *);
void primeFactors(int, int *);
double q02D(double, double, double, double);
void Velcoity(double *, double *, double **, double **, int, int, int, int, int);
void WriteSolution(double **, int, int, MPI_Decomp *);

int main(int argc, char **argv)
{

  int nx_tot, nx, i, ibeg, iend;
  int ny_tot, ny, j, jbeg, jend;
  int rank = 0, size = 1;
  int nstop = 1500;
  double xbeg = -M_PI, xend = M_PI;
  double ybeg = -M_PI, yend = M_PI;
  double dx = (xend - xbeg) / (NX_GLOB + 1), dtdx;
  double t = 0.0, dt;
  double dy = (yend - ybeg) / (NY_GLOB + 1), dtdy;
  double maxX, maxY;
  double *xg, *yg, *x, *y, **fm, **fp, **gm, **gp, **q, **ax, **ay, **axm, **axp, **aym, **ayp, **qnew;
  MPI_Decomp mpi_decomp;

  /* --------------------------------------------------------
     0. Initialize the MPI execution environment
     -------------------------------------------------------- */

#ifdef PARALLEL

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  DomainDecomposition(&mpi_decomp);
  nx = mpi_decomp.lsize[0];
  ny = mpi_decomp.lsize[1];
#else
  mpi_decomp.gsize[0] = mpi_decomp.lsize[0] = nx = NX_GLOB;
  mpi_decomp.gsize[1] = mpi_decomp.lsize[1] = ny = NY_GLOB;
  mpi_decomp.procL[0] = mpi_decomp.procL[1] = -1;
  mpi_decomp.procR[0] = mpi_decomp.procR[1] = -1;
#endif

#ifdef PARALLEL
  MPI_Barrier(MPI_COMM_WORLD);
#endif
  auto start = high_resolution_clock::now();

  /* --------------------------------------------------------
     1. Set local grid indices
     -------------------------------------------------------- */

  ibeg = NGHOST;
  iend = ibeg + nx - 1;
  nx = iend - ibeg + 1;
  nx_tot = nx + 2 * NGHOST;

  jbeg = NGHOST;
  jend = jbeg + ny - 1;
  ny = jend - jbeg + 1;
  ny_tot = ny + 2 * NGHOST;

  /* --------------------------------------------------------
     2. Generate global and local grids
     -------------------------------------------------------- */

  xg = (double *)malloc((NX_GLOB + 2 * NGHOST) * sizeof(double));
  yg = (double *)malloc((NY_GLOB + 2 * NGHOST) * sizeof(double));
  for (i = 0; i < (NX_GLOB + 2 * NGHOST); i++)
    xg[i] = xbeg + (i - ibeg + 1) * dx;
  for (j = 0; j < (NY_GLOB + 2 * NGHOST); j++)
    yg[j] = ybeg + (j - jbeg + 1) * dy;
#ifdef PARALLEL
  x = xg + mpi_decomp.start[0];
  y = yg + mpi_decomp.start[1];
#else
  x = xg;
  y = yg;
#endif
  /* --------------------------------------------------------
     3. Allocate memory on local processor and
        assign initial conditions.
     -------------------------------------------------------- */

  fm = Allocate_2DdblArray(nx_tot, ny_tot);
  fp = Allocate_2DdblArray(nx_tot, ny_tot);
  gm = Allocate_2DdblArray(nx_tot, ny_tot);
  gp = Allocate_2DdblArray(nx_tot, ny_tot);
  q = Allocate_2DdblArray(nx_tot, ny_tot);
  qnew = Allocate_2DdblArray(nx_tot, ny_tot);
  ax = Allocate_2DdblArray(nx_tot, ny_tot);
  ay = Allocate_2DdblArray(nx_tot, ny_tot);
  axm = Allocate_2DdblArray(nx_tot, ny_tot);
  axp = Allocate_2DdblArray(nx_tot, ny_tot);
  aym = Allocate_2DdblArray(nx_tot, ny_tot);
  ayp = Allocate_2DdblArray(nx_tot, ny_tot);
  double CFL = 0.9;

  for (i = ibeg; i <= iend; i++)
  {
    for (j = jbeg; j <= jend; j++)
    {
      q[i][j] = q02D(x[i], y[j], 0.0, 0.0);
    }
  }

  FILE *f, *timef;
  char str[128];

  /* --------------------------------------------------------
     4. Open log file
     -------------------------------------------------------- */

  if (rank == 0)
  {
    char logfile_name[128];
#ifdef PARALLEL
    sprintf(logfile_name, "outPc++/log.txt");
#else
    sprintf(logfile_name, "outSc++/log.txt");
#endif
    timef = fopen("outPc++/FileTime.txt", "w+");
    f = fopen(logfile_name, "w+");
    fprintf(f, "Grid = %03dx%03d, nstep = %04d\n\n", NX_GLOB, NY_GLOB, nstop);
  }

  /* --------------------------------------------------------
     5. Main iteration cycle
     -------------------------------------------------------- */
  for (int n = 0; n < nstop; n++)
  {

    /* -- 5a. Evaluate velocities from a prescribed function-- */
    Velcoity(x, y, ax, ay, ibeg, iend, jbeg, jend, rank);

    /* -- 5b. Evaluate adaptive dt-- */
    maxX = abs(ax[ibeg][jbeg]);
    maxY = abs(ay[ibeg][jbeg]);
    for (i = ibeg - NGHOST; i <= iend + NGHOST; i++)
    {
      for (j = jbeg - NGHOST; j <= jend + NGHOST; j++)
      {
        axm[i][j] = 0.5 * (ax[i][j] - abs(ax[i][j]));
        axp[i][j] = 0.5 * (ax[i][j] + abs(ax[i][j]));
        aym[i][j] = 0.5 * (ay[i][j] - abs(ay[i][j]));
        ayp[i][j] = 0.5 * (ay[i][j] + abs(ay[i][j]));
        maxX = MAX(maxX, abs(ax[i][j]));
        maxY = MAX(maxY, abs(ay[i][j]));
      }
    }
    double gmaxX, gmaxY;
#ifdef PARALLEL
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Allreduce(&maxX, &gmaxX, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    MPI_Allreduce(&maxY, &gmaxY, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
#else
    gmaxX = maxX;
    gmaxY = maxY;
#endif
    dt = CFL / (gmaxX / dx + gmaxY / dy);
    dtdx = dt / dx;
    dtdy = dt / dy;

    /* -- 5c. Apply boundary conditions-- */
    BoundaryConditions(q, x, y, nx, ny, &mpi_decomp);

    /* -- 5d. Evauate fluxes-- */
    for (i = ibeg; i <= iend + 1; i++)
    {
      for (j = jbeg; j <= jend; j++)
      {
        fm[i][j] = axm[i][j] * q[i][j] + axp[i][j] * q[i - 1][j];
        fp[i - 1][j] = axm[i - 1][j] * q[i][j] + axp[i - 1][j] * q[i - 1][j];
      }
    }
    for (i = ibeg; i <= iend; i++)
    {
      for (j = jbeg; j <= jend + 1; j++)
      {

        gm[i][j] = aym[i][j] * q[i][j] + ayp[i][j] * q[i][j - 1];
        gp[i][j - 1] = aym[i][j - 1] * q[i][j] + ayp[i][j - 1] * q[i][j - 1];
      }
    }

    /* -- 5e. Evaluate solution-- */
    for (j = jbeg; j <= jend; j++)
    {
      for (i = ibeg; i <= iend; i++)
      {
        qnew[i][j] = q[i][j] - dtdx * (fp[i][j] - fm[i][j]) - dtdy * (gp[i][j] - gm[i][j]);
      }
    }

    /* -- 5f. Write solution an logs-- */
    if (n % 300 == 0)
    {
      WriteSolution(q, nx, ny, &mpi_decomp);

      if (rank == 0)
      {
        sprintf(str, "%0.4e\n", t);
        fprintf(timef, "%s", str);
      }
    }

    double max_q = abs(q[ibeg][jbeg]);
    double min_q = abs(q[ibeg][jbeg]);
    for (i = ibeg; i <= iend; i++)
    {
      for (j = jbeg; j <= jend; j++)
      {

        max_q = MAX(max_q, abs(q[i][j]));
        min_q = MIN(min_q, abs(q[i][j]));
      }
    }

    double gmax_q, gmin_q;
#ifdef PARALLEL
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Allreduce(&max_q, &gmax_q, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    MPI_Allreduce(&min_q, &gmin_q, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
#else
    gmax_q = max_q;
    gmin_q = min_q;
#endif
    if (rank == 0)
    {
      sprintf(str, "n = %04d,  t = %0.8e,  dt =  %0.8e,  maxq =  %0.8e,  minq =  %0.8e\n", n, t, dt, gmax_q, gmin_q);
      printf("%s", str);
      fprintf(f, "%s", str);
    }

    /* -- 5d. Update time and solution-- */
    t = t + dt;
    for (i = ibeg; i <= iend; i++)
    {
      for (j = jbeg; j <= jend; j++)
      {
        q[i][j] = qnew[i][j];
      }
    }
  }
  /* --------------------------------------------------------
      6. Write one last time
      -------------------------------------------------------- */

  WriteSolution(q, nx, ny, &mpi_decomp);
  if (rank == 0)
  {
    sprintf(str, "%0.4e\n", t);
    fprintf(timef, "%s", str);
  }
/* --------------------------------------------------------
    7. Write execution time
    -------------------------------------------------------- */
#ifdef PARALLEL
  MPI_Barrier(MPI_COMM_WORLD);
#endif
  auto stop = high_resolution_clock::now();
  double time = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count() / 1000.0;
  if (rank == 0)
  {
    fclose(f);
    fclose(timef);
#ifdef PARALLEL
    f = fopen("outPc++/time.txt", "w");
#else
    f = fopen("outSc++/time.txt", "w");
#endif
    fprintf(f, "%0.3f\n", time);
    fclose(f);
  }
#ifdef PARALLEL
  MPI_Finalize();
#endif
  return 0;
}

#ifdef PARALLEL
/* ********************************************************************* */
void DomainDecomposition(MPI_Decomp *mpi_decomp)
/*
 * Decompose carteisian domain. Works for evry number of cores
 *
 *********************************************************************** */
{
  int dim, i;
  int rank, size;
  int *coords = mpi_decomp->coords;
  int *gsize = mpi_decomp->gsize;
  int *lsize = mpi_decomp->lsize;
  int *nprocs = mpi_decomp->nprocs;
  int *periods = mpi_decomp->periods;
  int *procL = mpi_decomp->procL;
  int *procR = mpi_decomp->procR;
  int *start = mpi_decomp->start;
  int new_coords[NDIM];

  /* --------------------------------------------------------
     1. Get rank & size
     -------------------------------------------------------- */

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  mpi_decomp->rank = rank;
  mpi_decomp->size = size;

  /* --------------------------------------------------------
     2. Obtain number of processor along each dimension.
     -------------------------------------------------------- */

  int arr[8];
  if (size != 1)
  {
    /* -- 2a. Factorize -- */
    primeFactors(size, arr);
    if (arr[0] == size)
    {
      /* -- 2b. Odd number of procs per direction -- */
      if (arr[0] % 2 == 0 && arr[0] != 2)
      {
        nprocs[0] = (int)sqrt(size);
        nprocs[1] = size / nprocs[0];
      }
      /* -- 2c. Prime number -- */
      else
      {
        nprocs[0] = arr[0];
        nprocs[1] = 1;
      }
    }
    /* -- 2d. Square grid -- */
    else if (arr[0] * arr[1] == size)
    {
      nprocs[0] = arr[0];
      nprocs[1] = arr[1];
    }
    /* -- 2e. Error -- */
    else
    {
      if (rank == 0)
        printf("! Cannot decompose\n");
      MPI_Finalize();
      exit(1);
    }
  }
  /* -- 2f. 1 proc -- */
  else
  {
    nprocs[0] = nprocs[1] = 1;
  }

  if (nprocs[1] > nprocs[0])
  {
    int tmp = nprocs[1];
    nprocs[1] = nprocs[0];
    nprocs[0] = tmp;
  }

  if (rank == 0)
  {
    printf("Decomposition achieved with %d X %d procs\n", nprocs[0], nprocs[1]);
  }

  periods[0] = 0;
  periods[1] = 0;

  /* --------------------------------------------------------
     3. Create Cartesian topology
     -------------------------------------------------------- */

  MPI_Cart_create(MPI_COMM_WORLD, NDIM, nprocs, periods,
                  0, &MPI_COMM_CART);
  MPI_Cart_get(MPI_COMM_CART, NDIM, nprocs, periods, coords);

  /* --------------------------------------------------------
     5. Determine ranks of neighbour processors
     -------------------------------------------------------- */

  for (dim = 0; dim < NDIM; dim++)
  {
    for (i = 0; i < NDIM; i++)
      new_coords[i] = coords[i];

    new_coords[dim] = coords[dim] + 1;
    if (new_coords[dim] < nprocs[dim])
    {
      MPI_Cart_rank(MPI_COMM_CART, new_coords, &(procR[dim]));
    }
    else
    {
      procR[dim] = MPI_PROC_NULL;
    }

    new_coords[dim] = coords[dim] - 1;
    if (new_coords[dim] >= 0)
    {
      MPI_Cart_rank(MPI_COMM_CART, new_coords, &(procL[dim]));
    }
    else
    {
      procL[dim] = MPI_PROC_NULL;
    }
  }
  /* --------------------------------------------------------
     4.Determine starting point of each rank
     -------------------------------------------------------- */

  gsize[0] = NX_GLOB;
  gsize[1] = NY_GLOB;
  lsize[0] = NX_GLOB / nprocs[0];
  lsize[1] = NY_GLOB / nprocs[1];

  /* -- 4a. If the integer division has left a reminder -- */
  if (nprocs[0] * lsize[0] != NX_GLOB)
    if (procL[0] < 0)
    {
      printf("NX = %d, nprocs*NX = %d\n", NX_GLOB, nprocs[0] * lsize[0]);
      lsize[0] += (NX_GLOB - nprocs[0] * lsize[0]);
    }
  if (nprocs[1] * lsize[1] != NY_GLOB)
    if (procL[1] < 0)
    {
      printf("NY = %d, nprocs*NY = %d\n", NY_GLOB, nprocs[1] * lsize[1]);
      lsize[1] += (NY_GLOB - nprocs[1] * lsize[1]);
    }

  int *dimsX = new int[size];
  int *dimsY = new int[size];

  /* -- 4b. Collect starting points of all ranks -- */
  int sndbuf = lsize[0];
  MPI_Allgather(&sndbuf, 1, MPI_INT, dimsX, 1, MPI_INT, MPI_COMM_WORLD);
  sndbuf = lsize[1];
  MPI_Allgather(&sndbuf, 1, MPI_INT, dimsY, 1, MPI_INT, MPI_COMM_WORLD);

  /* -- 4b. Fetch starting point of ranks in my row before me -- */
  for (int i = 0; i < (rank / nprocs[1]); i++)
  {
    start[0] += dimsX[i % nprocs[1] + i * nprocs[1]];
  }

  /* -- 4b. Fetch starting point of ranks in my column before me -- */
  for (int i = 0; i < (rank % nprocs[1]); i++)
  {
    start[1] += dimsY[rank - i - 1];
  }

  MPI_Barrier(MPI_COMM_WORLD);

  /* --------------------------------------------------------
     6. Print processor information.
        (Use MPI_Bcast() to print in sequence)
     -------------------------------------------------------- */

  int go, proc;
  for (proc = 0; proc < size; proc++)
  {
    go = proc;
    MPI_Bcast(&go, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (rank == go)
    {
      printf("[Rank %d]\n", rank);
      printf("  coords = [%d, %d], lsize = [%d, %d], start = [%d, %d]\n",
             coords[0], coords[1], lsize[0], lsize[1], start[0], start[1]);
      for (dim = 0; dim < NDIM; dim++)
      {
        printf("  (procL, procR)[%d] = %d, %d\n", dim, procL[dim], procR[dim]);
      }
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }
  delete dimsX, dimsY;
  return;
}
#endif

/* ********************************************************************* */
void BoundaryConditions(double **q, double *x, double *y, int nx, int ny, MPI_Decomp *mpi_decomp)
/*
 * No comunication is needed betwwen ranks. Only stationary global boundaries
 * are needed in this case.
 *
 *********************************************************************** */
{
  int i, j;
  int ibeg = NGHOST;
  int iend = ibeg + nx - 1;

  int jbeg = NGHOST;
  int jend = jbeg + ny - 1;

  int *procL = mpi_decomp->procL;
  int *procR = mpi_decomp->procR;
#ifdef PARALLEL
  double send_buf[NX_GLOB + 2 * NGHOST];
  double recv_buf[NX_GLOB + 2 * NGHOST];

  // Left buffer
  i = ibeg;
  for (j = jbeg; j <= jend; j++)
    send_buf[j] = q[i][j];
  MPI_Sendrecv(send_buf, jend + 1, MPI_DOUBLE, procL[0], 0,
               recv_buf, jend + 1, MPI_DOUBLE, procL[0], 0,
               MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  if (procL[0] >= 0)
    for (j = jbeg; j <= jend; j++)
      q[i - 1][j] = recv_buf[j];

  // Right buffer
  i = iend;
  for (j = jbeg; j <= jend; j++)
    send_buf[j] = q[i][j];
  MPI_Sendrecv(send_buf, jend + 1, MPI_DOUBLE, procR[0], 0,
               recv_buf, jend + 1, MPI_DOUBLE, procR[0], 0,
               MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  if (procR[0] >= 0)
    for (j = jbeg; j <= jend; j++)
      q[i + 1][j] = recv_buf[j];

  // Bottom buffer
  j = jbeg;
  for (i = ibeg; i <= iend; i++)
    send_buf[i] = q[i][j];
  MPI_Sendrecv(send_buf, iend + 1, MPI_DOUBLE, procL[1], 0,
               recv_buf, iend + 1, MPI_DOUBLE, procL[1], 0,
               MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  if (procL[1] >= 0)
    for (i = ibeg; i <= iend; i++)
      q[i][j - 1] = recv_buf[i];

  // Top buffer
  j = jend;
  for (i = ibeg; i <= iend; i++)
    send_buf[i] = q[i][j];
  MPI_Sendrecv(send_buf, iend + 1, MPI_DOUBLE, procR[1], 0,
               recv_buf, iend + 1, MPI_DOUBLE, procR[1], 0,
               MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  if (procR[1] >= 0)
    for (i = ibeg; i <= iend; i++)
      q[i][j + 1] = recv_buf[i];
#endif

  /* -- Left -- */
  if (procL[0] < 0)
  {
    i = ibeg - 1;
    for (j = jbeg; j <= jend; j++)
    {
      q[i][j] = q02D(x[i], y[j], 0, 0);
    }
  }

  /* -- Right -- */

  if (procR[0] < 0)
  {
    i = iend + 1;
    for (j = jbeg; j <= jend; j++)
    {
      q[i][j] = q02D(x[i], y[j], 0, 0);
    }
  }

  /* -- Bottom -- */
  if (procL[1] < 0)
  {
    j = jbeg - 1;
    for (i = ibeg; i <= iend; i++)
    {
      q[i][j] = q02D(x[i], y[j], 0, 0);
    }
  }

  /* -- Top -- */
  if (procR[1] < 0)
  {
    j = jend + 1;
    for (i = ibeg; i <= iend; i++)
    {
      q[i][j] = q02D(x[i], y[j], 0, 0);
    }
  }
}
/* ********************************************************************* */
void WriteSolution(double **q, int nx, int ny, MPI_Decomp *md)
/*
 * Write bin file in parallel or serial mode
 *
 *********************************************************************** */
{

  static int nfile = 0;
  char fname[32];

#ifdef PARALLEL

  sprintf(fname, "outPc++/Adv_%02d.bin", nfile);
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (rank == 0)
    printf("> Writing %s\n", fname);
  MPI_File fh;
  MPI_Datatype type_local, type_domain;
  int amode = MPI_MODE_CREATE | MPI_MODE_WRONLY;
  int gsize[2], lsize[2], start[2];

  /* --------------------------------------------------------
     1. Create a local array type without the ghost zones
        This datatype will be passed to MPI_File_write()
     -------------------------------------------------------- */

  gsize[0] = md->lsize[0] + 2 * NGHOST;
  gsize[1] = md->lsize[1] + 2 * NGHOST;

  lsize[0] = md->lsize[0];
  lsize[1] = md->lsize[1];

  start[0] = NGHOST;
  start[1] = NGHOST;

  MPI_Type_create_subarray(NDIM, gsize, lsize, start,
                           MPI_ORDER_C, MPI_DOUBLE, &type_local);
  MPI_Type_commit(&type_local);

  /* --------------------------------------------------------
     2. Create the subarry in the global domain.
        This datatype is used to set the file view.
     -------------------------------------------------------- */

  gsize[0] = NX_GLOB;
  gsize[1] = NY_GLOB;

  lsize[0] = md->lsize[0];
  lsize[1] = md->lsize[1];

  start[0] = md->start[0];
  start[1] = md->start[1];
  // start[0] = lsize[0] * md->coords[0]; // equal to md->start[0]
  // start[1] = lsize[1] * md->coords[1]; // equal to md->start[1]

  MPI_Type_create_subarray(NDIM, gsize, lsize, start,
                           MPI_ORDER_C, MPI_DOUBLE, &type_domain);
  MPI_Type_commit(&type_domain);

  /* --------------------------------------------------------
     3. Write to disk
     -------------------------------------------------------- */

  MPI_File_delete(fname, MPI_INFO_NULL);
  MPI_File_open(MPI_COMM_CART, fname, amode, MPI_INFO_NULL, &fh);
  MPI_File_set_view(fh, 0, MPI_DOUBLE, type_domain, "native", MPI_INFO_NULL);
  MPI_File_write_all(fh, q[0], 1, type_local, MPI_STATUS_IGNORE);
  MPI_File_close(&fh);
  MPI_Type_free(&type_local);
  MPI_Type_free(&type_domain);
#else

  sprintf(fname, "outSc++/Adv_%02d.bin", nfile);

  int i, j;
  int ibeg = NGHOST;
  int iend = ibeg + nx - 1;

  int jbeg = NGHOST;
  int jend = jbeg + ny - 1;
  printf("%d\n", jend - jbeg);
  FILE *fp;
  printf("> Writing %s\n", fname);
  fp = fopen(fname, "wb");

  for (j = jbeg; j <= jend; j++)
  {
    fwrite(q[j] + ibeg, sizeof(double), nx, fp);
  }

  fclose(fp);

#endif

  nfile++;
}

/* ********************************************************************* */
double q02D(double x, double y, double x0, double y0)
/*
 * Initial condition
 *
 *********************************************************************** */
{
  double a;
  a = exp(-8.0 * ((x - x0) * (x - x0) + (y + y0) * (y + y0)));
  return a;
  // if (x < 0.1 && x > -0.1 && y < 0.1 && y > -0.1)
  //   return 1.0;
  // else
  //   return 0.0;
}

/* ********************************************************************* */
void Velcoity(double *x, double *y, double **ax, double **ay, int ibeg, int iend, int jbeg, int jend, int rank)
/*
 * Analitical prescriotion for the advection velocity
 *
 *********************************************************************** */
{
  for (int i = ibeg - NGHOST; i <= iend + NGHOST; i++)
  {
    for (int j = jbeg - NGHOST; j <= jend + NGHOST; j++)
    {
      ax[i][j] = sin(x[i]) * cos(y[j]);
    }
  }
  for (int i = ibeg - NGHOST; i <= iend + NGHOST; i++)
  {
    for (int j = jbeg - NGHOST; j <= jend + NGHOST; j++)
    {
      ay[i][j] = -cos(x[i]) * sin(y[j]);
    }
  }
}

/* ********************************************************************* */
void primeFactors(int n, int *arr)
/*
 * Tool to factorize numbers
 *
 *********************************************************************** */
{
  int k = 0, j = 0;
  while (n % 2 == 0)
  {
    j = 1;
    k++;
    n = n / 2;
  }
  if (k != 0)
  {
    arr[0] = pow(2, k);
  }

  for (int i = 3; i <= sqrt(n); i = i + 2)
  {
    while (n % i == 0)
    {
      arr[j++] = i;
      n = n / i;
    }
  }

  if (n > 2)
    arr[j++] = n;
#ifdef PARALLEL
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#endif

  if (j > 2)
  {
#ifdef PARALLEL
    if (rank == 0)
      printf("Error in prime numbers!\n\n");
    MPI_Finalize();
#endif
    exit(1);
  }
}
