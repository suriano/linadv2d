To compile the c++ code in its serial version it is sufficient to run

$ g++ parAdv.cpp

and run with

$ ./a.out

The parallel executable is obtained via

$ mpicxx −DPARALLEL −lm −std=c++11 parAdv.cpp

and have to be run using

$ mpirun − ncores ./a.out

where cores is the number of processors you want to use.
In the directory is present a simple script to run the time analysis.
It is sufficient to have the code complied with mpicxx and typing

$ . run_parallel.sh

In the folder it is also present the Jupyter notebook plots.ipynb to analyse the results.
