% Upwind scheme for the 2D scalar linear advection
clear;
clc;

tic;
% domain:
xL =-pi;
xR = pi;
yL =-pi;
yR = pi;

t    = 0;
nstop = 5000;
NGHOST = 1;
Nx = 1024 + 2*NGHOST;
Ny = Nx;

x = zeros(Nx);%linspace(xL,xR,Nx); % mesh in  the x-direction
y = zeros(Ny);%linspace(yL,yR,Ny); % mesh in  the y-direction

fm = zeros(Nx,Ny);   % f_{i-1/2}  see the video recodrings 
fp = zeros(Nx,Ny);   % f_{i+1/2}
gm = zeros(Nx,Ny);   % g_{i-1/2}
gp = zeros(Nx,Ny);   % g_{i+1/2}

dx = (xR - xL) / (Nx + 1 - 2*NGHOST);%x(2) - x(1);
dy = (yR - yL) / (Ny + 1 - 2*NGHOST);%y(2) - y(1);
CFL = 0.9;

for i=0:Nx-1
    x(i+1) = xL + i*dx;
end

for i=0:Ny-1
    y(i+1) = yL + i*dy;
end
% set initial data
q = zeros(Nx,Ny);
for i = 1:Nx
    for j=1:Ny
        q(i,j) = q02D(x(i),y(j),0,0);
    end
end

% log filed
file_number = 0;
logfile_name = 'outSMatlab/log.txt';
f = fopen(logfile_name, 'w+');
fprintf(f, 'Grid = %03dx%03d, nstep = %04d\n\n', Nx, Ny, nstop);

% time loop 
for n=0:nstop

    [ax,ay] = Velocity(x,y,t,Nx,Ny);

    axm = 0.5*(ax - abs(ax)); 
    axp = 0.5*(ax + abs(ax)); 
    aym = 0.5*(ay - abs(ay)); 
    ayp = 0.5*(ay + abs(ay));
    dt  = CFL/( max(max(abs(ax)))/dx + max(max(abs(ay)))/dy ) ;
    
    %begin boundary condiutions, assuming 1 GHOST zone
    fm(1 ,:) = axm(1,:).*q(1,:) + axp(Nx,:).*q(Nx,:);
    fp(Nx,:) = axm(1,:).*q(1,:) + axp(Nx,:).*q(Nx,:);


    gm(:,1 ) = aym(:,1).*q(:,1) + ayp(:,Ny).*q(:,Ny);
    gp(:,Ny) = aym(:,1).*q(:,1) + ayp(:,Ny).*q(:,Ny);
    %end boundary conditions
    

    fm(1+NGHOST:Nx,:) = axm(1+NGHOST:Nx,:).*q(1+NGHOST:Nx,:) + axp(1+NGHOST:Nx,:).*q(1:Nx-NGHOST,:);
    fp(1:Nx-NGHOST,:) = axm(1:Nx-NGHOST,:).*q(1+NGHOST:Nx,:) + axp(1:Nx-NGHOST,:).*q(1:Nx-NGHOST,:);


    gm(:,1+NGHOST:Ny) = aym(:,1+NGHOST:Ny).*q(:,1+NGHOST:Ny) + ayp(:,1+NGHOST:Ny).*q(:,1:Ny-NGHOST);
    gp(:,1:Ny-NGHOST) = aym(:,1:Ny-NGHOST).*q(:,1+NGHOST:Ny) + ayp(:,1:Ny-NGHOST).*q(:,1:Ny-NGHOST);


    qnew = q - dt/dx*( fp - fm ) - dt/dy*( gp - gm );
    
    %print data ones in a while
    if(mod(n, 500) == 0)
        % Write data to text file
        file_name = 'outSMatlab/data%02d.txt';
        file_name = sprintf(file_name, file_number);
        writematrix(q(2:Nx-1,2:Ny-1), file_name);
        fprintf("> Writing file data%02d.txt\n", file_number);
        file_number = file_number + 1;

    end

    %print caracteristic quantities
    max_q = max(max(q));
    min_q = min(min(q));
    str = 'n = %04d, t = %0.8e, dt = %0.8f, maxq = %0.8e, minq = %0.8e, meanq = %0.8e\n';

    fprintf(f, str, n, t, dt, max_q, min_q);

    % update time and solution
    t = t + dt;
    q = qnew;
end
time = toc;
fclose(f);

f = fopen("outSMatlab/time.txt", 'w');
fprintf(f, '%0.5e\n', time);
fclose(f);
disp(time);
%*********************************************************
% Auxilary function
function q = q02D(x,y,x0,y0)
% initial condition for the linear advection PDE

% test 1
q = exp(-8*((x-x0)^2 + (y+y0)^2));

% test2
% if (x < 0.1 && x >-0.1 && y < 0.1 && y>-0.1)
%     q = 1;
% else
%     q = 0;
% end

end

%****************************************************
function [ax,ay] = Velocity(x,y,time,Nx,Ny)
% analytical function for the advection velocities
nu = 0;
ax = zeros(Nx,Ny);
ay = zeros(Nx,Ny);
% u
for i=1:Nx
    for j=1:Ny
        ax(i,j) = sin(x(i))*cos(y(j));%*exp(-2*nu*time);
    end
end
   
% v
for j = 1:Ny
    for i=1:Nx
        ay(i,j) =-cos(x(i))*sin(y(j));%*exp(-2*nu*time);
    end
end

end